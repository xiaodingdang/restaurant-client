﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace client
{
    class Dish
    {
        public string id;
        public string name;
        public string picture;
        public string price;
        public string type;
        public override string ToString()
        {
            return name + "   " + price;
        }
    }
}
