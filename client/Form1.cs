﻿using MetroFramework.Controls;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace client
{
    public partial class Form1 : MetroForm
    {
        public static Form1 makeForm(string type)
        {
            Form1 form1 = null;
            switch (type)
            {
                case "2":
                    form1 = new Form1();
                    form1.waiterInit();
                    break;
                case "3":
                    form1 = new Form1();
                    form1.receptionInit();
                    break;
                case "4":
                    form1 = new Form1();
                    form1.cookInit();
                    break;
                default:
                    break;
            }
            return form1;
        }


        Form1()
        {
            InitializeComponent();
            operationTabControl.HideTab(tablePage);
            operationTabControl.HideTab(orderPage);
            operationTabControl.HideTab(chargePage);
            operationTabControl.HideTab(cookingPage);
        }


        void waiterInit()
        {
            Text = "服务员";
            operationTabControl.ShowTab(orderPage);
            operationTabControl.ShowTab(tablePage);
            operationTabControl.SelectTab(tablePage);

            dishListRefresh();
            orderRefresh("0");
            tableRefresh();
        }
        void receptionInit()
        {
            Text = "前台";
            orderButton.Text = "结账";
            orderButton.Click -= orderButton_Click;
            orderButton.Click += chargeButton_Click;

            operationTabControl.ShowTab(tablePage);
            operationTabControl.ShowTab(chargePage);
            operationTabControl.SelectTab(tablePage);
            tableRefresh();
        }
        void cookInit()
        {
            Text = "厨师";
            cookingPageInit();
            operationTabControl.ShowTab(cookingPage);
            operationTabControl.SelectTab(cookingPage);
        }
        void tableRefresh()
        {
            Table[] tables = Tool.getTables();
            tableListView.Items.Clear();
            for (int i = 0; i < tables.Length; i++)
            {
                var table = tables[i];
                ListViewItem item = new ListViewItem(table.tableName, tableListView.Groups[int.Parse(table.status)]);
                item.Tag = table;
                item.ImageIndex = int.Parse(table.status);
                item.Name = table.tableName;
                tableListView.Items.Add(item);
            }
        }

        void orderRefresh(string tableNum)
        {
            orderListBox.Items.Clear();
            priceLabel.Text = "0";
            orderTableNumLabel.Text = tableNum;
        }
        void cookingPageInit()
        {
            cookListRefresh();
        }
        void cookListRefresh()
        {
            cookListView.Items.Clear();
            Dish[] dishes = Tool.getCookDishes();
            for (int i = 0; i < dishes.Length; i++)
            {
                var dish = dishes[i];
                String[] subItems = new string[2];
                subItems[0] = dish.id;
                subItems[1] = dish.name;
                ListViewItem item = new ListViewItem(subItems);
                item.Tag = dish;
                item.Name = dish.name;
                cookListView.Items.Add(item);
            }
        }
        void dishListRefresh()
        {
            Dish[] dishes = Tool.getDishes();
            ImageList imageList = menuListView.LargeImageList;
            imageList.Images.Clear();
            for (int i = 0; i < dishes.Length; i++)
            {
                var dish = dishes[i];
                imageList.Images.Add(Tool.getDishImage(dish));
                String[] subItems = new string[3];
                subItems[0] = dish.id;
                subItems[1] = dish.name;
                subItems[2] = dish.price;
                ListViewItem item = new ListViewItem(subItems, i, menuListView.Groups[int.Parse(dish.type)]);
                item.Tag = dish;
                item.ImageIndex = imageList.Images.Count - 1;
                item.Name = dish.name;
                menuListView.Items.Add(item);
            }
            menuListView.Columns[0].Width = imageList.ImageSize.Width;
        }
        void orderDelete()
        {
            priceLabel.Text = (float.Parse(priceLabel.Text) - float.Parse(((Dish)orderListBox.Items[orderListBox.SelectedIndex]).price)).ToString();
            orderListBox.Items.RemoveAt(orderListBox.SelectedIndex);
        }
        void orderAdd(Dish dish)
        {
            orderListBox.Items.Add(dish);
            priceLabel.Text = (float.Parse(priceLabel.Text) + float.Parse(dish.price)).ToString();
        }

        private void filterButtonClicked(object sender, EventArgs e)
        {
            MetroTile tile = (MetroTile)sender;
            ListView.ListViewItemCollection items = null;
            switch (operationTabControl.SelectedTab.Name)
            {
                case "tablePage":
                    items = tableListView.Groups[tile.Tag.ToString()].Items; break;
                case "orderPage":
                    items = menuListView.Groups[tile.Tag.ToString()].Items; break;
                default:
                    break;
            }
            if (items.Count != 0)
            {
                items[0].EnsureVisible();
            }
        }

        private void orderListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                orderAdd((Dish)e.Item.Tag);
                e.Item.Selected = false;
            }
        }

        private void orderListView_Click(object sender, EventArgs e)
        {
            ListView listView = (ListView)sender;

        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            orderDelete();
        }

        private void orderListBox_ControlAdded(object sender, ControlEventArgs e)
        {
        }

        private void commitButton_Click(object sender, EventArgs e)
        {
            var items = orderListBox.Items;
            if (orderTableNumLabel.Text != "0" && items.Count != 0)
            {
                Dish[] dishes = new Dish[orderListBox.Items.Count];
                for (int i = 0; i < items.Count; i++)
                {
                    dishes[i] = (Dish)items[i];
                }
                Tool.sendOrder((Table)tableDescriptionPanel.Tag, dishes);
                orderListBox.Items.Clear();
                priceLabel.Text = "0";
                MessageBox.Show(this, "提交成功！");
            }
        }

        private void tableListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                selectItem(e.Item);
                e.Item.Selected = false;
            }
        }
        void selectItem(ListViewItem item)
        {
            Table table = (Table)item.Tag;
            item.Group = tableListView.Groups[int.Parse(table.status)];
            tableDescriptionPanel.Tag = table;
            selectedTableNumLabel.Text = table.tableName;
            seatsCountLabel.Text = table.seatsCount;
            useedLabel.Text = item.Group.Header;
            if (table.status != "2")
            {
                selectTableButton.Style = MetroFramework.MetroColorStyle.Default;
                selectTableButton.Enabled = true;
                orderButton.Style = MetroFramework.MetroColorStyle.Silver;
                orderButton.Enabled = false;
            }
            else
            {
                selectTableButton.Style = MetroFramework.MetroColorStyle.Silver;
                selectTableButton.Enabled = false;
                orderButton.Style = MetroFramework.MetroColorStyle.Default;
                orderButton.Enabled = true;
            }
            tableListView.Sort();
        }

        void setListContent(ListView list)
        {

        }
        private void selectTableButton_Click(object sender, EventArgs e)
        {
            Table table = (Table)tableDescriptionPanel.Tag;
            bool flag = Tool.selectTable(table);
            if (flag)
            {
                MessageBox.Show(this, "上座成功！", "提示");
            }
            else
            {
                MessageBox.Show(this, "上座失败！", "提示");
            }
            tableRefresh();
            var item = tableListView.Items[table.tableName];

            selectItem(item);
        }

        private void orderButton_Click(object sender, EventArgs e)
        {
            operationTabControl.SelectTab(orderPage);
            orderRefresh(selectedTableNumLabel.Text);
        }
        private void chargeButton_Click(object sender, EventArgs e)
        {
            operationTabControl.SelectTab(chargePage);
            double totalPrice = 0;
            Table table = (Table)tableDescriptionPanel.Tag;
            chargeTableNumLabel.Text = table.tableName;
            Order order = Tool.getOrder(table.id);
            chargeList.Tag = order;
            Dish[] dishes = order.dishes;
            foreach (Dish dish in dishes)
            {
                chargeList.Items.Add(dish);
                totalPrice += double.Parse(dish.price);
            }
            chargeTotalPriceLabel.Text = totalPrice.ToString();
        }

        private void comfirmCharge_Click(object sender, EventArgs e)
        {
            if (Tool.payOrder(((Order)(chargeList.Tag)).order, ((Table)(tableDescriptionPanel.Tag)).id, chargeTotalPriceLabel.Text, (string)chargeMethodPanel.Tag))
            {
                chargeList.Items.Clear();
                chargeTableNumLabel.Text = "0";
                chargeTotalPriceLabel.Text = "0";
                chargeList.Tag = null;
                tableRefresh();
            }
            else
            {
                MessageBox.Show("失败！");
            }
        }

        private void checkedChanged(object sender, EventArgs e)
        {
            chargeMethodPanel.Tag = ((RadioButton)sender).Tag;
        }


        private void ListView_SizeChanged(object sender, EventArgs e)
        {
            ListView list = (ListView)sender;
            for (int i = 1; i < list.Columns.Count; i++)
            {
                list.Columns[i].Width = list.Width - list.Columns[0].Width / (list.Columns.Count - 1);
            }
        }


        private void selectDish(object sender, EventArgs e)
        {
            if (Tool.changeCookDishStatus((Dish)(cookListView.SelectedItems[0].Tag)))
            {
                cookButton.Click -= selectDish;
                cookButton.Click += finishDish;
                cookButton.Text = "完成";
                cookListView.Enabled = false;
            }
        }
        private void finishDish(object sender, EventArgs e)
        {
            if (Tool.changeCookDishStatus((Dish)(cookListView.SelectedItems[0].Tag)))
            {
                cookButton.Click -= finishDish;
                cookButton.Click += selectDish;
                cookButton.Text = "选择";
                cookListView.Enabled = true;
                cookListRefresh();
                cookedDishLabel.Text = "";
            }
        }

        private void cookListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                cookedDishLabel.Text = cookListView.SelectedItems[0].SubItems[1].Text;
            }
        }
    }
}
