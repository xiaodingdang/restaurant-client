﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace client
{
    class PageFactory
    {
        public static TabPage makePage(string type) {
            FlowLayoutPanel panel=null;
            switch (type)
            {
                case "table":
                    TabPage tablePage = new TabPage("餐桌");
                    panel = new FlowLayoutPanel();
                    tablePage.Controls.Add(panel);
                    return tablePage;
                    break;
                case "order":
                    TabPage orderPage = new TabPage("点餐");
                    panel = new FlowLayoutPanel();
                    orderPage.Controls.Add(panel);
                    return orderPage;
                    break;
                case "cook":
                    break;
                case "charge":
                    break;
                case "preorder":
                    break;
                default:
                    break;
            }
            return null;
        }
    }
}
