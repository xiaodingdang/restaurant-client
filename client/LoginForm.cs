﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace client
{
    public partial class LoginForm : MetroForm
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            string r = Tool.login(userNameBox.Text, passwordBox.Text);
            Form1 form1 = Form1.makeForm(r);
            if (form1==null)
            {
                tipLabel.Text = "登陆失败";
            }
            else
            {
                Hide();
                form1.ShowDialog();
                Close();
            }
        }
    }
}
