﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace client
{
    class Tool
    {
        static string target = "http://47.95.212.18";
        static string orderUri = "http://47.95.212.18/php/staff/order.php";
        static string cookUri = "http://47.95.212.18/php/staff/cook.php";
        static string tableUri = "http://47.95.212.18/php/staff/table.php";
        static string loginUri = "http://47.95.212.18/php/staff/login.php";

        public static string login(string usr, string pw)
        {
            //http
            JObject json = new JObject();
            json.Add("request", "login");
            json.Add("id", usr);
            json.Add("password", pw);
            string jstring = json.ToString();
            JObject jObject = sendHTTP(jstring, loginUri);
            try
            { return jObject.GetValue("data").ToObject<string>(); }
            catch (Exception)
            {
                return null;
            }
        }
        public static Table[] getTables()
        {
            JObject json = new JObject();
            json.Add("request", JProperty.FromObject("getTables"));
            string jstring = json.ToString();
            Table[] tables = null;
            JObject jObject = sendHTTP(jstring, tableUri);
            //string flag = jObject.GetValue("message").ToObject<string>();
            try
            {
                tables = jObject.GetValue("data").ToObject<Table[]>();
                return tables;
            }
            catch (Exception)
            {
                return new Table[0];
            }

        }
        public static Dish[] getDishes()
        {
            Dish[] dishes = null;
            JObject json = new JObject();
            json.Add("request", JProperty.FromObject("getMenu"));
            string jstring = json.ToString();
            JObject jObject = sendHTTP(jstring, orderUri);
            if (judge(jObject))
            {
                dishes = jObject.GetValue("data").ToObject<Dish[]>();
                return dishes;
            }
            else
            {
                return new Dish[0];
            }
        }
        public static Dish[] getCookDishes()
        {
            Dish[] dishes = null;
            JObject json = new JObject();
            json.Add("request", JProperty.FromObject("getCookDishs"));
            string jstring = json.ToString();
            JObject jObject = sendHTTP(jstring, cookUri);

            if (judge(jObject))
            {
                string data = jObject.GetValue("data").ToString();
                if (data!=null)
                {
                    JArray jArray = JArray.Parse(data);
                    dishes = new Dish[jArray.Count];
                    for (int i = 0; i < jArray.Count; i++)
                    {
                        var item = jArray[i];
                        Dish dish = new Dish();
                        dish.id = item["id"].ToString();
                        dish.name = item["name"].ToString();
                        dishes[i] = dish;
                    }
                }
            }
            if (dishes == null)
            {
                return new Dish[0];
            }
            return dishes;
        }
        public static bool sendOrder(Table table, Dish[] dishes)
        {
            string jsonDishes = JsonConvert.SerializeObject(dishes);
            string jsonTable = JsonConvert.SerializeObject(table);
            JObject json = new JObject();
            json.Add("request", JProperty.FromObject("createOrder"));
            json.Add("dishes", JArray.Parse(jsonDishes));
            json.Add("table", JObject.Parse(jsonTable));
            String jstring = json.ToString();
            try
            {
                JObject jObject = sendHTTP(jstring, orderUri);
                return judge(jObject);
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static Image getDishImage(Dish dish)
        {
            Image image = null;
            try
            {
                string directory = dish.picture;
                directory = target + directory.Substring(directory.IndexOf("/src"));
                HttpWebRequest request = HttpWebRequest.Create(directory) as HttpWebRequest;
                request.Method = "GET";
                var response = request.GetResponse();
                using (var stream = response.GetResponseStream())
                {
                    image = Image.FromStream(stream);
                }
            }
            catch (Exception)
            {
                image = Image.FromFile("d://frog.bmp");
            }
            return image;
        }
        public static bool payOrder(string order_id, string table_id, string total_price, string method)
        {
            JObject json = new JObject();
            JObject order = new JObject();
            order.Add("order_id", order_id);
            order.Add("table_id", table_id);
            order.Add("total_price", total_price);
            json.Add("request", "payOrder");
            json.Add("order", order);
            json.Add("method", method);
            JObject jObject = sendHTTP(json.ToString(), orderUri);
            return judge(jObject);
        }
        public static Order getOrder(string tableID)
        {
            JObject json = new JObject();
            json.Add("request", "getOrder");
            json.Add("table", tableID);
            JObject jObject = sendHTTP(json.ToString(), orderUri);
            Order order = new Order();

            string id = jObject.GetValue("order").ToObject<string>();
            Dish[] dishes = jObject.GetValue("dishes").ToObject<Dish[]>();
            order.order = id;
            order.dishes = dishes;
            return order;
        }
        public static bool selectTable(Table table)
        {
            //发送上座请求和table
            //服务器发回是否成功，以及修改后的table
            JObject json = new JObject();
            json.Add("request", "useTable");
            json.Add("id", table.id);
            try
            {
                JObject jObject = sendHTTP(json.ToString(), tableUri);
                return judge(jObject);
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool changeCookDishStatus(Dish dish)
        {
            try
            {
                JObject json = new JObject();
                json.Add("request", "changeCookStatus");
                JObject jt = new JObject();
                jt.Add("id", dish.id);
                jt.Add("name", dish.name);
                json.Add("dish", jt);

                JObject jObject = sendHTTP(json.ToString(), cookUri);
                return judge(jObject);
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool judge(JObject jObject)
        {
            try
            {
                if (jObject == null || jObject.GetValue("message").ToObject<string>() == "error")
                {
                    return false;
                }
                else if (jObject.GetValue("message").ToObject<string>() == "success")
                {
                    return true;
                }

            }
            catch (Exception)
            {
            }
            return false;
        }
        private static JObject sendHTTP(string jstring, string uri)
        {
            HttpWebRequest request = HttpWebRequest.Create(uri) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            jstring = System.Web.HttpUtility.UrlEncode("param") + "=" + System.Web.HttpUtility.UrlEncode(jstring);
            byte[] bs = Encoding.UTF8.GetBytes(jstring);
            request.ContentLength = bs.Length;
            JObject jObject = null;
            try
            {
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(bs, 0, bs.Length);
                    reqStream.Close();
                }
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                byte[] rbs = new byte[10000/*response.ContentLength*/];
                using (Stream resStream = response.GetResponseStream())
                {
                    jstring = "";
                    int num;
                    while ((num = resStream.Read(rbs, 0, rbs.Length)) != 0)
                    {
                        jstring += Encoding.UTF8.GetString(rbs, 0, num);
                    }

                    jObject = JObject.Parse(jstring);
                    resStream.Close();
                }
                response.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("error");
            }
            return jObject;
        }
    }
}
