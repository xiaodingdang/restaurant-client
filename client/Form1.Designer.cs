﻿namespace client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("空桌", System.Windows.Forms.HorizontalAlignment.Center);
            System.Windows.Forms.ListViewGroup listViewGroup5 = new System.Windows.Forms.ListViewGroup("已预订", System.Windows.Forms.HorizontalAlignment.Center);
            System.Windows.Forms.ListViewGroup listViewGroup6 = new System.Windows.Forms.ListViewGroup("已上座", System.Windows.Forms.HorizontalAlignment.Center);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.ListViewGroup listViewGroup9 = new System.Windows.Forms.ListViewGroup("其他", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup10 = new System.Windows.Forms.ListViewGroup("早餐", System.Windows.Forms.HorizontalAlignment.Center);
            System.Windows.Forms.ListViewGroup listViewGroup11 = new System.Windows.Forms.ListViewGroup("主食", System.Windows.Forms.HorizontalAlignment.Center);
            System.Windows.Forms.ListViewGroup listViewGroup12 = new System.Windows.Forms.ListViewGroup("甜品和饮料", System.Windows.Forms.HorizontalAlignment.Center);
            System.Windows.Forms.ListViewGroup listViewGroup13 = new System.Windows.Forms.ListViewGroup("小食", System.Windows.Forms.HorizontalAlignment.Center);
            this.operationTabControl = new MetroFramework.Controls.MetroTabControl();
            this.tablePage = new MetroFramework.Controls.MetroTabPage();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.tableListView = new System.Windows.Forms.ListView();
            this.tableImageList = new System.Windows.Forms.ImageList(this.components);
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.metroTile5 = new MetroFramework.Controls.MetroTile();
            this.metroTile4 = new MetroFramework.Controls.MetroTile();
            this.metroTile3 = new MetroFramework.Controls.MetroTile();
            this.tableDescriptionPanel = new System.Windows.Forms.TableLayoutPanel();
            this.orderButton = new MetroFramework.Controls.MetroTile();
            this.label7 = new System.Windows.Forms.Label();
            this.useedLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.seatsCountLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.selectedTableNumLabel = new System.Windows.Forms.Label();
            this.selectTableButton = new MetroFramework.Controls.MetroTile();
            this.label9 = new System.Windows.Forms.Label();
            this.orderPage = new MetroFramework.Controls.MetroTabPage();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.menuListView = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dishNameColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dishPriceColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dishImageList = new System.Windows.Forms.ImageList(this.components);
            this.dishListPanel = new System.Windows.Forms.Panel();
            this.orderListBox = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.commitButton = new MetroFramework.Controls.MetroTile();
            this.deleteButton = new MetroFramework.Controls.MetroTile();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.breakfastButton = new MetroFramework.Controls.MetroTile();
            this.mainCourseButton = new MetroFramework.Controls.MetroTile();
            this.snackButton = new MetroFramework.Controls.MetroTile();
            this.label3 = new System.Windows.Forms.Label();
            this.priceLabel = new System.Windows.Forms.Label();
            this.dessertAndDrinkButton = new MetroFramework.Controls.MetroTile();
            this.orderTableNumLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cookingPage = new MetroFramework.Controls.MetroTabPage();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.cookListView = new System.Windows.Forms.ListView();
            this.idHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1 = new System.Windows.Forms.Panel();
            this.cookedDishLabel = new System.Windows.Forms.Label();
            this.chargePage = new MetroFramework.Controls.MetroTabPage();
            this.chargePanel = new MetroFramework.Controls.MetroPanel();
            this.chargeList = new System.Windows.Forms.ListBox();
            this.chargeMethodPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.cashButton = new System.Windows.Forms.RadioButton();
            this.alipayButton = new System.Windows.Forms.RadioButton();
            this.wechatButton = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.chargeTableNumLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comfirmCharge = new MetroFramework.Controls.MetroTile();
            this.chargeTotalPriceLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cookButton = new System.Windows.Forms.Button();
            this.operationTabControl.SuspendLayout();
            this.tablePage.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableDescriptionPanel.SuspendLayout();
            this.orderPage.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.dishListPanel.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.cookingPage.SuspendLayout();
            this.metroPanel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.chargePage.SuspendLayout();
            this.chargePanel.SuspendLayout();
            this.chargeMethodPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // operationTabControl
            // 
            this.operationTabControl.Controls.Add(this.tablePage);
            this.operationTabControl.Controls.Add(this.orderPage);
            this.operationTabControl.Controls.Add(this.cookingPage);
            this.operationTabControl.Controls.Add(this.chargePage);
            this.operationTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.operationTabControl.FontSize = MetroFramework.MetroTabControlSize.Tall;
            this.operationTabControl.ItemSize = new System.Drawing.Size(150, 40);
            this.operationTabControl.Location = new System.Drawing.Point(20, 60);
            this.operationTabControl.Name = "operationTabControl";
            this.operationTabControl.SelectedIndex = 1;
            this.operationTabControl.Size = new System.Drawing.Size(858, 473);
            this.operationTabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.operationTabControl.TabIndex = 4;
            this.operationTabControl.UseSelectable = true;
            // 
            // tablePage
            // 
            this.tablePage.Controls.Add(this.metroPanel2);
            this.tablePage.Font = new System.Drawing.Font("宋体", 9F);
            this.tablePage.HorizontalScrollbarBarColor = true;
            this.tablePage.HorizontalScrollbarHighlightOnWheel = false;
            this.tablePage.HorizontalScrollbarSize = 0;
            this.tablePage.Location = new System.Drawing.Point(4, 44);
            this.tablePage.Name = "tablePage";
            this.tablePage.Size = new System.Drawing.Size(850, 425);
            this.tablePage.TabIndex = 0;
            this.tablePage.Text = "查看餐桌";
            this.tablePage.VerticalScrollbarBarColor = true;
            this.tablePage.VerticalScrollbarHighlightOnWheel = false;
            this.tablePage.VerticalScrollbarSize = 0;
            // 
            // metroPanel2
            // 
            this.metroPanel2.Controls.Add(this.tableListView);
            this.metroPanel2.Controls.Add(this.tableLayoutPanel4);
            this.metroPanel2.Controls.Add(this.tableDescriptionPanel);
            this.metroPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(0, 0);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(850, 425);
            this.metroPanel2.TabIndex = 2;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // tableListView
            // 
            this.tableListView.Dock = System.Windows.Forms.DockStyle.Fill;
            listViewGroup4.Header = "空桌";
            listViewGroup4.HeaderAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            listViewGroup4.Name = "free";
            listViewGroup5.Header = "已预订";
            listViewGroup5.HeaderAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            listViewGroup5.Name = "ordered";
            listViewGroup6.Header = "已上座";
            listViewGroup6.HeaderAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            listViewGroup6.Name = "inUse";
            this.tableListView.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup4,
            listViewGroup5,
            listViewGroup6});
            this.tableListView.LargeImageList = this.tableImageList;
            this.tableListView.Location = new System.Drawing.Point(0, 62);
            this.tableListView.Name = "tableListView";
            this.tableListView.Size = new System.Drawing.Size(611, 363);
            this.tableListView.SmallImageList = this.tableImageList;
            this.tableListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.tableListView.TabIndex = 3;
            this.tableListView.UseCompatibleStateImageBehavior = false;
            this.tableListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.tableListView_ItemSelectionChanged);
            // 
            // tableImageList
            // 
            this.tableImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("tableImageList.ImageStream")));
            this.tableImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.tableImageList.Images.SetKeyName(0, "emptyTable.bmp");
            this.tableImageList.Images.SetKeyName(1, "emptyTable.bmp");
            this.tableImageList.Images.SetKeyName(2, "fullTable.bmp");
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.Controls.Add(this.metroTile5, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.metroTile4, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.metroTile3, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(611, 62);
            this.tableLayoutPanel4.TabIndex = 5;
            // 
            // metroTile5
            // 
            this.metroTile5.ActiveControl = null;
            this.metroTile5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTile5.Location = new System.Drawing.Point(409, 3);
            this.metroTile5.Name = "metroTile5";
            this.metroTile5.Size = new System.Drawing.Size(199, 56);
            this.metroTile5.TabIndex = 10;
            this.metroTile5.Tag = "inUse";
            this.metroTile5.Text = "已上座";
            this.metroTile5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile5.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTile5.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile5.UseSelectable = true;
            this.metroTile5.Click += new System.EventHandler(this.filterButtonClicked);
            // 
            // metroTile4
            // 
            this.metroTile4.ActiveControl = null;
            this.metroTile4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTile4.Location = new System.Drawing.Point(206, 3);
            this.metroTile4.Name = "metroTile4";
            this.metroTile4.Size = new System.Drawing.Size(197, 56);
            this.metroTile4.TabIndex = 9;
            this.metroTile4.Tag = "ordered";
            this.metroTile4.Text = "已预定";
            this.metroTile4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile4.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTile4.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile4.UseSelectable = true;
            this.metroTile4.Click += new System.EventHandler(this.filterButtonClicked);
            // 
            // metroTile3
            // 
            this.metroTile3.ActiveControl = null;
            this.metroTile3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTile3.Location = new System.Drawing.Point(3, 3);
            this.metroTile3.Name = "metroTile3";
            this.metroTile3.Size = new System.Drawing.Size(197, 56);
            this.metroTile3.TabIndex = 8;
            this.metroTile3.Tag = "free";
            this.metroTile3.Text = "空桌";
            this.metroTile3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile3.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTile3.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile3.UseSelectable = true;
            this.metroTile3.Click += new System.EventHandler(this.filterButtonClicked);
            // 
            // tableDescriptionPanel
            // 
            this.tableDescriptionPanel.AutoScroll = true;
            this.tableDescriptionPanel.ColumnCount = 2;
            this.tableDescriptionPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableDescriptionPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableDescriptionPanel.Controls.Add(this.orderButton, 1, 4);
            this.tableDescriptionPanel.Controls.Add(this.label7, 0, 3);
            this.tableDescriptionPanel.Controls.Add(this.useedLabel, 1, 3);
            this.tableDescriptionPanel.Controls.Add(this.label5, 0, 2);
            this.tableDescriptionPanel.Controls.Add(this.seatsCountLabel, 1, 2);
            this.tableDescriptionPanel.Controls.Add(this.label2, 0, 1);
            this.tableDescriptionPanel.Controls.Add(this.selectedTableNumLabel, 1, 1);
            this.tableDescriptionPanel.Controls.Add(this.selectTableButton, 0, 4);
            this.tableDescriptionPanel.Controls.Add(this.label9, 0, 0);
            this.tableDescriptionPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.tableDescriptionPanel.Location = new System.Drawing.Point(611, 0);
            this.tableDescriptionPanel.Name = "tableDescriptionPanel";
            this.tableDescriptionPanel.RowCount = 5;
            this.tableDescriptionPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableDescriptionPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableDescriptionPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableDescriptionPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableDescriptionPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableDescriptionPanel.Size = new System.Drawing.Size(239, 425);
            this.tableDescriptionPanel.TabIndex = 4;
            // 
            // orderButton
            // 
            this.orderButton.ActiveControl = null;
            this.orderButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.orderButton.Enabled = false;
            this.orderButton.Location = new System.Drawing.Point(122, 343);
            this.orderButton.Name = "orderButton";
            this.orderButton.Size = new System.Drawing.Size(114, 79);
            this.orderButton.Style = MetroFramework.MetroColorStyle.Silver;
            this.orderButton.TabIndex = 8;
            this.orderButton.Text = "点菜";
            this.orderButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.orderButton.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.orderButton.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.orderButton.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.orderButton.UseSelectable = true;
            this.orderButton.Click += new System.EventHandler(this.orderButton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(3, 255);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 85);
            this.label7.TabIndex = 4;
            this.label7.Text = "使用情况：";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // useedLabel
            // 
            this.useedLabel.AutoSize = true;
            this.useedLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.useedLabel.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.useedLabel.Location = new System.Drawing.Point(122, 255);
            this.useedLabel.Name = "useedLabel";
            this.useedLabel.Size = new System.Drawing.Size(114, 85);
            this.useedLabel.TabIndex = 5;
            this.useedLabel.Text = "          ";
            this.useedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(3, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 85);
            this.label5.TabIndex = 2;
            this.label5.Text = "座位数：";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // seatsCountLabel
            // 
            this.seatsCountLabel.AutoSize = true;
            this.seatsCountLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.seatsCountLabel.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.seatsCountLabel.Location = new System.Drawing.Point(122, 170);
            this.seatsCountLabel.Name = "seatsCountLabel";
            this.seatsCountLabel.Size = new System.Drawing.Size(114, 85);
            this.seatsCountLabel.TabIndex = 3;
            this.seatsCountLabel.Text = "           ";
            this.seatsCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(3, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 85);
            this.label2.TabIndex = 0;
            this.label2.Text = "桌号：";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // selectedTableNumLabel
            // 
            this.selectedTableNumLabel.AutoSize = true;
            this.selectedTableNumLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectedTableNumLabel.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.selectedTableNumLabel.Location = new System.Drawing.Point(122, 85);
            this.selectedTableNumLabel.Name = "selectedTableNumLabel";
            this.selectedTableNumLabel.Size = new System.Drawing.Size(114, 85);
            this.selectedTableNumLabel.TabIndex = 1;
            this.selectedTableNumLabel.Text = "            ";
            this.selectedTableNumLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // selectTableButton
            // 
            this.selectTableButton.ActiveControl = null;
            this.selectTableButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectTableButton.Enabled = false;
            this.selectTableButton.Location = new System.Drawing.Point(3, 343);
            this.selectTableButton.Name = "selectTableButton";
            this.selectTableButton.Size = new System.Drawing.Size(113, 79);
            this.selectTableButton.Style = MetroFramework.MetroColorStyle.Silver;
            this.selectTableButton.TabIndex = 7;
            this.selectTableButton.Text = "上座";
            this.selectTableButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.selectTableButton.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.selectTableButton.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.selectTableButton.UseSelectable = true;
            this.selectTableButton.Click += new System.EventHandler(this.selectTableButton_Click);
            // 
            // label9
            // 
            this.tableDescriptionPanel.SetColumnSpan(this.label9, 2);
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(233, 85);
            this.label9.TabIndex = 6;
            this.label9.Text = "餐桌描述：";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // orderPage
            // 
            this.orderPage.Controls.Add(this.metroPanel1);
            this.orderPage.HorizontalScrollbarBarColor = true;
            this.orderPage.HorizontalScrollbarHighlightOnWheel = false;
            this.orderPage.HorizontalScrollbarSize = 0;
            this.orderPage.Location = new System.Drawing.Point(4, 44);
            this.orderPage.Name = "orderPage";
            this.orderPage.Size = new System.Drawing.Size(850, 425);
            this.orderPage.TabIndex = 1;
            this.orderPage.Text = "点菜";
            this.orderPage.VerticalScrollbarBarColor = true;
            this.orderPage.VerticalScrollbarHighlightOnWheel = false;
            this.orderPage.VerticalScrollbarSize = 0;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.menuListView);
            this.metroPanel1.Controls.Add(this.dishListPanel);
            this.metroPanel1.Controls.Add(this.tableLayoutPanel2);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(850, 425);
            this.metroPanel1.TabIndex = 2;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // menuListView
            // 
            this.menuListView.Activation = System.Windows.Forms.ItemActivation.TwoClick;
            this.menuListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.dishNameColumn,
            this.dishPriceColumn});
            this.menuListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.menuListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuListView.FullRowSelect = true;
            this.menuListView.GridLines = true;
            listViewGroup9.Header = "其他";
            listViewGroup9.Name = "others";
            listViewGroup10.Header = "早餐";
            listViewGroup10.HeaderAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            listViewGroup10.Name = "breakfast";
            listViewGroup11.Header = "主食";
            listViewGroup11.HeaderAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            listViewGroup11.Name = "mainCourse";
            listViewGroup12.Header = "甜品和饮料";
            listViewGroup12.HeaderAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            listViewGroup12.Name = "dessertAndDrink";
            listViewGroup13.Header = "小食";
            listViewGroup13.HeaderAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            listViewGroup13.Name = "snack";
            this.menuListView.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup9,
            listViewGroup10,
            listViewGroup11,
            listViewGroup12,
            listViewGroup13});
            this.menuListView.LargeImageList = this.dishImageList;
            this.menuListView.Location = new System.Drawing.Point(0, 56);
            this.menuListView.MultiSelect = false;
            this.menuListView.Name = "menuListView";
            this.menuListView.Size = new System.Drawing.Size(559, 369);
            this.menuListView.SmallImageList = this.dishImageList;
            this.menuListView.TabIndex = 0;
            this.menuListView.UseCompatibleStateImageBehavior = false;
            this.menuListView.View = System.Windows.Forms.View.Details;
            this.menuListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.orderListView_ItemSelectionChanged);
            this.menuListView.SizeChanged += new System.EventHandler(this.ListView_SizeChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "图片";
            this.columnHeader3.Width = 111;
            // 
            // dishNameColumn
            // 
            this.dishNameColumn.Text = "菜名";
            this.dishNameColumn.Width = 233;
            // 
            // dishPriceColumn
            // 
            this.dishPriceColumn.Text = "价格";
            this.dishPriceColumn.Width = 212;
            // 
            // dishImageList
            // 
            this.dishImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit;
            this.dishImageList.ImageSize = new System.Drawing.Size(100, 100);
            this.dishImageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // dishListPanel
            // 
            this.dishListPanel.BackColor = System.Drawing.SystemColors.Control;
            this.dishListPanel.Controls.Add(this.orderListBox);
            this.dishListPanel.Controls.Add(this.tableLayoutPanel3);
            this.dishListPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.dishListPanel.Location = new System.Drawing.Point(559, 56);
            this.dishListPanel.Name = "dishListPanel";
            this.dishListPanel.Size = new System.Drawing.Size(291, 369);
            this.dishListPanel.TabIndex = 9;
            // 
            // orderListBox
            // 
            this.orderListBox.BackColor = System.Drawing.SystemColors.Menu;
            this.orderListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.orderListBox.Font = new System.Drawing.Font("微软雅黑", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.orderListBox.FormattingEnabled = true;
            this.orderListBox.ItemHeight = 57;
            this.orderListBox.Location = new System.Drawing.Point(0, 0);
            this.orderListBox.Name = "orderListBox";
            this.orderListBox.Size = new System.Drawing.Size(291, 309);
            this.orderListBox.TabIndex = 4;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.commitButton, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.deleteButton, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 309);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(291, 60);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // commitButton
            // 
            this.commitButton.ActiveControl = null;
            this.commitButton.BackColor = System.Drawing.SystemColors.Control;
            this.commitButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.commitButton.Location = new System.Drawing.Point(148, 3);
            this.commitButton.Name = "commitButton";
            this.commitButton.Size = new System.Drawing.Size(140, 54);
            this.commitButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.commitButton.TabIndex = 8;
            this.commitButton.Tag = "snack";
            this.commitButton.Text = "完成点餐";
            this.commitButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.commitButton.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.commitButton.UseSelectable = true;
            this.commitButton.Click += new System.EventHandler(this.commitButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.ActiveControl = null;
            this.deleteButton.BackColor = System.Drawing.SystemColors.Control;
            this.deleteButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deleteButton.Location = new System.Drawing.Point(3, 3);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(139, 54);
            this.deleteButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.deleteButton.TabIndex = 9;
            this.deleteButton.Tag = "snack";
            this.deleteButton.Text = "删除";
            this.deleteButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.deleteButton.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.deleteButton.UseSelectable = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 8;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.Controls.Add(this.breakfastButton, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.mainCourseButton, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.snackButton, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.priceLabel, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.dessertAndDrinkButton, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.orderTableNumLabel, 7, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 6, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(850, 56);
            this.tableLayoutPanel2.TabIndex = 8;
            // 
            // breakfastButton
            // 
            this.breakfastButton.ActiveControl = null;
            this.breakfastButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.breakfastButton.Location = new System.Drawing.Point(3, 3);
            this.breakfastButton.Name = "breakfastButton";
            this.breakfastButton.Size = new System.Drawing.Size(100, 50);
            this.breakfastButton.TabIndex = 18;
            this.breakfastButton.Tag = "breakfast";
            this.breakfastButton.Text = "早餐";
            this.breakfastButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.breakfastButton.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.breakfastButton.UseSelectable = true;
            this.breakfastButton.Click += new System.EventHandler(this.filterButtonClicked);
            // 
            // mainCourseButton
            // 
            this.mainCourseButton.ActiveControl = null;
            this.mainCourseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainCourseButton.Location = new System.Drawing.Point(109, 3);
            this.mainCourseButton.Name = "mainCourseButton";
            this.mainCourseButton.Size = new System.Drawing.Size(100, 50);
            this.mainCourseButton.TabIndex = 19;
            this.mainCourseButton.Tag = "mainCourse";
            this.mainCourseButton.Text = "主食";
            this.mainCourseButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mainCourseButton.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.mainCourseButton.UseSelectable = true;
            this.mainCourseButton.Click += new System.EventHandler(this.filterButtonClicked);
            // 
            // snackButton
            // 
            this.snackButton.ActiveControl = null;
            this.snackButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.snackButton.Location = new System.Drawing.Point(215, 3);
            this.snackButton.Name = "snackButton";
            this.snackButton.Size = new System.Drawing.Size(100, 50);
            this.snackButton.TabIndex = 21;
            this.snackButton.Tag = "snack";
            this.snackButton.Text = "小食";
            this.snackButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.snackButton.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.snackButton.UseSelectable = true;
            this.snackButton.Click += new System.EventHandler(this.filterButtonClicked);
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(427, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 56);
            this.label3.TabIndex = 24;
            this.label3.Text = "总价：";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // priceLabel
            // 
            this.priceLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.priceLabel.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.priceLabel.Location = new System.Drawing.Point(533, 0);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(100, 56);
            this.priceLabel.TabIndex = 25;
            this.priceLabel.Text = "0";
            this.priceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dessertAndDrinkButton
            // 
            this.dessertAndDrinkButton.ActiveControl = null;
            this.dessertAndDrinkButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dessertAndDrinkButton.Location = new System.Drawing.Point(321, 3);
            this.dessertAndDrinkButton.Name = "dessertAndDrinkButton";
            this.dessertAndDrinkButton.Size = new System.Drawing.Size(100, 50);
            this.dessertAndDrinkButton.TabIndex = 20;
            this.dessertAndDrinkButton.Tag = "dessertAndDrink";
            this.dessertAndDrinkButton.Text = "甜品和饮料";
            this.dessertAndDrinkButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.dessertAndDrinkButton.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.dessertAndDrinkButton.UseSelectable = true;
            this.dessertAndDrinkButton.Click += new System.EventHandler(this.filterButtonClicked);
            // 
            // orderTableNumLabel
            // 
            this.orderTableNumLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.orderTableNumLabel.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.orderTableNumLabel.Location = new System.Drawing.Point(745, 0);
            this.orderTableNumLabel.Name = "orderTableNumLabel";
            this.orderTableNumLabel.Size = new System.Drawing.Size(102, 56);
            this.orderTableNumLabel.TabIndex = 23;
            this.orderTableNumLabel.Text = "0";
            this.orderTableNumLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(639, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 56);
            this.label1.TabIndex = 22;
            this.label1.Text = "桌号：";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cookingPage
            // 
            this.cookingPage.Controls.Add(this.metroPanel3);
            this.cookingPage.HorizontalScrollbarBarColor = true;
            this.cookingPage.HorizontalScrollbarHighlightOnWheel = false;
            this.cookingPage.HorizontalScrollbarSize = 0;
            this.cookingPage.Location = new System.Drawing.Point(4, 44);
            this.cookingPage.Name = "cookingPage";
            this.cookingPage.Size = new System.Drawing.Size(850, 425);
            this.cookingPage.TabIndex = 5;
            this.cookingPage.Text = "做菜";
            this.cookingPage.VerticalScrollbarBarColor = true;
            this.cookingPage.VerticalScrollbarHighlightOnWheel = false;
            this.cookingPage.VerticalScrollbarSize = 0;
            // 
            // metroPanel3
            // 
            this.metroPanel3.Controls.Add(this.cookListView);
            this.metroPanel3.Controls.Add(this.panel1);
            this.metroPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(0, 0);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(850, 425);
            this.metroPanel3.TabIndex = 3;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // cookListView
            // 
            this.cookListView.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.cookListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.idHeader,
            this.nameHeader});
            this.cookListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.cookListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cookListView.Font = new System.Drawing.Font("宋体", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cookListView.FullRowSelect = true;
            this.cookListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.cookListView.HideSelection = false;
            this.cookListView.Location = new System.Drawing.Point(0, 0);
            this.cookListView.MultiSelect = false;
            this.cookListView.Name = "cookListView";
            this.cookListView.Size = new System.Drawing.Size(850, 351);
            this.cookListView.TabIndex = 0;
            this.cookListView.UseCompatibleStateImageBehavior = false;
            this.cookListView.View = System.Windows.Forms.View.Details;
            this.cookListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.cookListView_ItemSelectionChanged);
            // 
            // idHeader
            // 
            this.idHeader.Text = "菜号";
            this.idHeader.Width = 160;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cookedDishLabel);
            this.panel1.Controls.Add(this.cookButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 351);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(850, 74);
            this.panel1.TabIndex = 11;
            // 
            // cookedDishLabel
            // 
            this.cookedDishLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cookedDishLabel.Font = new System.Drawing.Font("宋体", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cookedDishLabel.Location = new System.Drawing.Point(0, 0);
            this.cookedDishLabel.Name = "cookedDishLabel";
            this.cookedDishLabel.Size = new System.Drawing.Size(635, 74);
            this.cookedDishLabel.TabIndex = 11;
            this.cookedDishLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chargePage
            // 
            this.chargePage.Controls.Add(this.chargePanel);
            this.chargePage.HorizontalScrollbarBarColor = true;
            this.chargePage.HorizontalScrollbarHighlightOnWheel = false;
            this.chargePage.HorizontalScrollbarSize = 0;
            this.chargePage.Location = new System.Drawing.Point(4, 44);
            this.chargePage.Name = "chargePage";
            this.chargePage.Size = new System.Drawing.Size(850, 425);
            this.chargePage.TabIndex = 3;
            this.chargePage.Text = "收款";
            this.chargePage.VerticalScrollbarBarColor = true;
            this.chargePage.VerticalScrollbarHighlightOnWheel = false;
            this.chargePage.VerticalScrollbarSize = 0;
            // 
            // chargePanel
            // 
            this.chargePanel.Controls.Add(this.chargeList);
            this.chargePanel.Controls.Add(this.chargeMethodPanel);
            this.chargePanel.Controls.Add(this.tableLayoutPanel1);
            this.chargePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chargePanel.HorizontalScrollbarBarColor = true;
            this.chargePanel.HorizontalScrollbarHighlightOnWheel = false;
            this.chargePanel.HorizontalScrollbarSize = 10;
            this.chargePanel.Location = new System.Drawing.Point(0, 0);
            this.chargePanel.Name = "chargePanel";
            this.chargePanel.Size = new System.Drawing.Size(850, 425);
            this.chargePanel.TabIndex = 2;
            this.chargePanel.VerticalScrollbarBarColor = true;
            this.chargePanel.VerticalScrollbarHighlightOnWheel = false;
            this.chargePanel.VerticalScrollbarSize = 10;
            // 
            // chargeList
            // 
            this.chargeList.BackColor = System.Drawing.SystemColors.Menu;
            this.chargeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chargeList.Font = new System.Drawing.Font("微软雅黑", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chargeList.FormattingEnabled = true;
            this.chargeList.ItemHeight = 57;
            this.chargeList.Location = new System.Drawing.Point(0, 60);
            this.chargeList.Name = "chargeList";
            this.chargeList.Size = new System.Drawing.Size(567, 365);
            this.chargeList.TabIndex = 10;
            // 
            // chargeMethodPanel
            // 
            this.chargeMethodPanel.Controls.Add(this.cashButton);
            this.chargeMethodPanel.Controls.Add(this.alipayButton);
            this.chargeMethodPanel.Controls.Add(this.wechatButton);
            this.chargeMethodPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.chargeMethodPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.chargeMethodPanel.Location = new System.Drawing.Point(567, 60);
            this.chargeMethodPanel.Name = "chargeMethodPanel";
            this.chargeMethodPanel.Size = new System.Drawing.Size(283, 365);
            this.chargeMethodPanel.TabIndex = 11;
            this.chargeMethodPanel.Tag = "1";
            // 
            // cashButton
            // 
            this.cashButton.AutoSize = true;
            this.cashButton.Checked = true;
            this.cashButton.Font = new System.Drawing.Font("宋体", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cashButton.Location = new System.Drawing.Point(3, 3);
            this.cashButton.Name = "cashButton";
            this.cashButton.Size = new System.Drawing.Size(118, 44);
            this.cashButton.TabIndex = 8;
            this.cashButton.TabStop = true;
            this.cashButton.Tag = "1";
            this.cashButton.Text = "现金";
            this.cashButton.UseVisualStyleBackColor = true;
            this.cashButton.CheckedChanged += new System.EventHandler(this.checkedChanged);
            // 
            // alipayButton
            // 
            this.alipayButton.AutoSize = true;
            this.alipayButton.Font = new System.Drawing.Font("宋体", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.alipayButton.Location = new System.Drawing.Point(3, 53);
            this.alipayButton.Name = "alipayButton";
            this.alipayButton.Size = new System.Drawing.Size(158, 44);
            this.alipayButton.TabIndex = 9;
            this.alipayButton.Tag = "2";
            this.alipayButton.Text = "支付宝";
            this.alipayButton.UseVisualStyleBackColor = true;
            this.alipayButton.CheckedChanged += new System.EventHandler(this.checkedChanged);
            // 
            // wechatButton
            // 
            this.wechatButton.AutoSize = true;
            this.wechatButton.Font = new System.Drawing.Font("宋体", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wechatButton.Location = new System.Drawing.Point(3, 103);
            this.wechatButton.Name = "wechatButton";
            this.wechatButton.Size = new System.Drawing.Size(198, 44);
            this.wechatButton.TabIndex = 10;
            this.wechatButton.Tag = "3";
            this.wechatButton.Text = "微信支付";
            this.wechatButton.UseVisualStyleBackColor = true;
            this.wechatButton.CheckedChanged += new System.EventHandler(this.checkedChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.chargeTableNumLabel, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.comfirmCharge, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.chargeTotalPriceLabel, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(850, 60);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // chargeTableNumLabel
            // 
            this.chargeTableNumLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chargeTableNumLabel.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chargeTableNumLabel.Location = new System.Drawing.Point(513, 0);
            this.chargeTableNumLabel.Name = "chargeTableNumLabel";
            this.chargeTableNumLabel.Size = new System.Drawing.Size(164, 60);
            this.chargeTableNumLabel.TabIndex = 30;
            this.chargeTableNumLabel.Text = "0";
            this.chargeTableNumLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(343, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(164, 60);
            this.label6.TabIndex = 29;
            this.label6.Text = "桌号：";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comfirmCharge
            // 
            this.comfirmCharge.ActiveControl = null;
            this.comfirmCharge.BackColor = System.Drawing.SystemColors.Control;
            this.comfirmCharge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comfirmCharge.Location = new System.Drawing.Point(683, 3);
            this.comfirmCharge.Name = "comfirmCharge";
            this.comfirmCharge.Size = new System.Drawing.Size(164, 54);
            this.comfirmCharge.Style = MetroFramework.MetroColorStyle.Blue;
            this.comfirmCharge.TabIndex = 28;
            this.comfirmCharge.Tag = "";
            this.comfirmCharge.Text = "确认收款";
            this.comfirmCharge.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.comfirmCharge.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.comfirmCharge.UseSelectable = true;
            this.comfirmCharge.Click += new System.EventHandler(this.comfirmCharge_Click);
            // 
            // chargeTotalPriceLabel
            // 
            this.chargeTotalPriceLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chargeTotalPriceLabel.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chargeTotalPriceLabel.Location = new System.Drawing.Point(173, 0);
            this.chargeTotalPriceLabel.Name = "chargeTotalPriceLabel";
            this.chargeTotalPriceLabel.Size = new System.Drawing.Size(164, 60);
            this.chargeTotalPriceLabel.TabIndex = 26;
            this.chargeTotalPriceLabel.Text = "0";
            this.chargeTotalPriceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(164, 60);
            this.label4.TabIndex = 25;
            this.label4.Text = "总价：";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nameHeader
            // 
            this.nameHeader.Text = "菜名";
            this.nameHeader.Width = 726;
            // 
            // cookButton
            // 
            this.cookButton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.cookButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.cookButton.Font = new System.Drawing.Font("微软雅黑", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cookButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cookButton.Location = new System.Drawing.Point(635, 0);
            this.cookButton.Name = "cookButton";
            this.cookButton.Size = new System.Drawing.Size(215, 74);
            this.cookButton.TabIndex = 12;
            this.cookButton.Text = "选择";
            this.cookButton.UseVisualStyleBackColor = false;
            this.cookButton.Click += new System.EventHandler(this.selectDish);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(898, 553);
            this.Controls.Add(this.operationTabControl);
            this.Name = "Form1";
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.operationTabControl.ResumeLayout(false);
            this.tablePage.ResumeLayout(false);
            this.metroPanel2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableDescriptionPanel.ResumeLayout(false);
            this.tableDescriptionPanel.PerformLayout();
            this.orderPage.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            this.dishListPanel.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.cookingPage.ResumeLayout(false);
            this.metroPanel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.chargePage.ResumeLayout(false);
            this.chargePanel.ResumeLayout(false);
            this.chargeMethodPanel.ResumeLayout(false);
            this.chargeMethodPanel.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroTabControl operationTabControl;
        private MetroFramework.Controls.MetroTabPage chargePage;
        private MetroFramework.Controls.MetroTabPage cookingPage;
        private MetroFramework.Controls.MetroTabPage orderPage;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private System.Windows.Forms.ListView menuListView;
        private System.Windows.Forms.ImageList dishImageList;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader dishNameColumn;
        private System.Windows.Forms.ColumnHeader dishPriceColumn;
        private System.Windows.Forms.ImageList tableImageList;
        private MetroFramework.Controls.MetroTabPage tablePage;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private System.Windows.Forms.ListView tableListView;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private MetroFramework.Controls.MetroTile metroTile5;
        private MetroFramework.Controls.MetroTile metroTile4;
        private MetroFramework.Controls.MetroTile metroTile3;
        private MetroFramework.Controls.MetroPanel chargePanel;
        private System.Windows.Forms.ListBox chargeList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label chargeTotalPriceLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton cashButton;
        private System.Windows.Forms.FlowLayoutPanel chargeMethodPanel;
        private System.Windows.Forms.RadioButton alipayButton;
        private System.Windows.Forms.RadioButton wechatButton;
        private MetroFramework.Controls.MetroTile comfirmCharge;
        private System.Windows.Forms.TableLayoutPanel tableDescriptionPanel;
        private MetroFramework.Controls.MetroTile orderButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label useedLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label seatsCountLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label selectedTableNumLabel;
        private MetroFramework.Controls.MetroTile selectTableButton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label chargeTableNumLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroTile breakfastButton;
        private MetroFramework.Controls.MetroTile mainCourseButton;
        private MetroFramework.Controls.MetroTile snackButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label priceLabel;
        private MetroFramework.Controls.MetroTile dessertAndDrinkButton;
        private System.Windows.Forms.Label orderTableNumLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel dishListPanel;
        private System.Windows.Forms.ListBox orderListBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private MetroFramework.Controls.MetroTile commitButton;
        private MetroFramework.Controls.MetroTile deleteButton;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private System.Windows.Forms.ListView cookListView;
        private System.Windows.Forms.ColumnHeader idHeader;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label cookedDishLabel;
        private System.Windows.Forms.ColumnHeader nameHeader;
        private System.Windows.Forms.Button cookButton;
    }
}